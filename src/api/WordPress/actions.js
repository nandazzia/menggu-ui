import request from '@/utils/request'

// 查询通知列表
export function listActions(query) {
  return request({
    url: '/WordPress/actions/list',
    method: 'get',
    params: query
  })
}

// 查询通知详细
export function getActions(actionId) {
  return request({
    url: '/WordPress/actions/' + actionId,
    method: 'get'
  })
}

// 新增通知
export function addActions(data) {
  return request({
    url: '/WordPress/actions',
    method: 'post',
    data: data
  })
}

// 修改通知
export function updateActions(data) {
  return request({
    url: '/WordPress/actions',
    method: 'put',
    data: data
  })
}

// 删除通知
export function delActions(actionId) {
  return request({
    url: '/WordPress/actions/' + actionId,
    method: 'delete'
  })
}
