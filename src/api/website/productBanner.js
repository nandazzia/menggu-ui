import request from '@/utils/request'

// 查询产品轮播管理列表
export function listProductBanner(query) {
  return request({
    url: '/website/productBanner/list',
    method: 'get',
    params: query
  })
}

// 查询产品轮播管理详细
export function getProductBanner(proId) {
  return request({
    url: '/website/productBanner/' + proId,
    method: 'get'
  })
}

// 新增产品轮播管理

export function addProductBanner(data) {
  return request({
    url: '/website/productBanner',
    method: 'post',
    data: data
  })
}

// 修改产品轮播管理

export function updateProductBanner(data) {
  return request({
    url: '/website/productBanner',
    method: 'put',
    data: data
  })
}

// 删除产品轮播管理

export function delProductBanner(proId) {
  return request({
    url: '/website/productBanner/' + proId,
    method: 'delete'
  })
}
