import request from '@/utils/request'

// 查询模板信息列表
export function listTemplate(query) {
  return request({
    url: '/website/template/list',
    method: 'get',
    params: query
  })
}

// 查询模板信息详细
export function getTemplate(templateId) {
  return request({
    url: '/website/template/' + templateId,
    method: 'get'
  })
}

// 新增模板信息
export function addTemplate(data) {
  return request({
    url: '/website/template',
    method: 'post',
    data: data
  })
}

// 修改模板信息
export function updateTemplate(data) {
  return request({
    url: '/website/template',
    method: 'put',
    data: data
  })
}

// 删除模板信息
export function delTemplate(templateId) {
  return request({
    url: '/website/template/' + templateId,
    method: 'delete'
  })
}
