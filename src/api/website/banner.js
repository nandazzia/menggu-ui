import request from '@/utils/request'

// 查询官网轮播图数组列表
export function listBanner(query) {
  return request({
    url: '/website/banner/list',
    method: 'get',
    params: query
  })
}

// 查询官网轮播图数组详细
export function getBanner(bannerId) {
  return request({
    url: '/website/banner/' + bannerId,
    method: 'get'
  })
}

// 新增官网轮播图数组
export function addBanner(data) {
  return request({
    url: '/website/banner',
    method: 'post',
    data: data
  })
}

// 修改官网轮播图数组
export function updateBanner(data) {
  return request({
    url: '/website/banner',
    method: 'put',
    data: data
  })
}

// 删除官网轮播图数组
export function delBanner(bannerId) {
  return request({
    url: '/website/banner/' + bannerId,
    method: 'delete'
  })
}
