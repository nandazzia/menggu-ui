import request from '@/utils/request'

// 查询sloganCard列表
export function listSloganCard(query) {
  return request({
    url: '/website/sloganCard/list',
    method: 'get',
    params: query
  })
}

// 查询sloganCard详细
export function getSloganCard(cardId) {
  return request({
    url: '/website/sloganCard/' + cardId,
    method: 'get'
  })
}

// 新增sloganCard
export function addSloganCard(data) {
  return request({
    url: '/website/sloganCard',
    method: 'post',
    data: data
  })
}

// 修改sloganCard
export function updateSloganCard(data) {
  return request({
    url: '/website/sloganCard',
    method: 'put',
    data: data
  })
}

// 删除sloganCard
export function delSloganCard(cardId) {
  return request({
    url: '/website/sloganCard/' + cardId,
    method: 'delete'
  })
}
