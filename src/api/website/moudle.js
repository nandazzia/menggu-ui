import request from '@/utils/request'

// 查询模块控制列表
export function listMoudle(query) {
  return request({
    url: '/website/moudle/list',
    method: 'get',
    params: query
  })
}

// 查询模块控制详细
export function getMoudle(moudleId) {
  return request({
    url: '/website/moudle/' + moudleId,
    method: 'get'
  })
}

// 新增模块控制
export function addMoudle(data) {
  return request({
    url: '/website/moudle',
    method: 'post',
    data: data
  })
}

// 修改模块控制
export function updateMoudle(data) {
  return request({
    url: '/website/moudle',
    method: 'put',
    data: data
  })
}

// 删除模块控制
export function delMoudle(moudleId) {
  return request({
    url: '/website/moudle/' + moudleId,
    method: 'delete'
  })
}
