import request from '@/utils/request'

// 查询行业痛点列表
export function listPain(query) {
  return request({
    url: '/website/pain/list',
    method: 'get',
    params: query
  })
}

// 查询行业痛点详细
export function getPain(id) {
  return request({
    url: '/website/pain/' + id,
    method: 'get'
  })
}

// 新增行业痛点
export function addPain(data) {
  return request({
    url: '/website/pain',
    method: 'post',
    data: data
  })
}

// 修改行业痛点
export function updatePain(data) {
  return request({
    url: '/website/pain',
    method: 'put',
    data: data
  })
}

// 删除行业痛点
export function delPain(id) {
  return request({
    url: '/website/pain/' + id,
    method: 'delete'
  })
}
