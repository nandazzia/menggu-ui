import request from '@/utils/request'

// 查询首页的四个代性的解决方案列表
export function listSolutionCard(query) {
  return request({
    url: '/website/solutionCard/list',
    method: 'get',
    params: query
  })
}

// 查询首页的四个代性的解决方案详细
export function getSolutionCard(solutionId) {
  return request({
    url: '/website/solutionCard/' + solutionId,
    method: 'get'
  })
}

// 新增首页的四个代性的解决方案
export function addSolutionCard(data) {
  return request({
    url: '/website/solutionCard',
    method: 'post',
    data: data
  })
}

// 修改首页的四个代性的解决方案
export function updateSolutionCard(data) {
  return request({
    url: '/website/solutionCard',
    method: 'put',
    data: data
  })
}

// 删除首页的四个代性的解决方案
export function delSolutionCard(solutionId) {
  return request({
    url: '/website/solutionCard/' + solutionId,
    method: 'delete'
  })
}
