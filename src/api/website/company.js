import request from '@/utils/request'

// 查询企业基本信息列表
export function listCompany(query) {
  return request({
    url: '/website/company/list',
    method: 'get',
    params: query
  })
}
// 查询企业基本信息列表 全查
export function listCompanyByAll(query) {
  return request({
    url: '/website/company/listByAll',
    method: 'get',
    params: query
  })
}
// 查询企业基本信息详细
export function getCompany(id) {
  return request({
    url: '/website/company/' + id,
    method: 'get'
  })
}

// 新增企业基本信息
export function addCompany(data) {
  return request({
    url: '/website/company',
    method: 'post',
    data: data
  })
}

// 修改企业基本信息
export function updateCompany(data) {
  return request({
    url: '/website/company',
    method: 'put',
    data: data
  })
}

// 删除企业基本信息
export function delCompany(id) {
  return request({
    url: '/website/company/' + id,
    method: 'delete'
  })
}
