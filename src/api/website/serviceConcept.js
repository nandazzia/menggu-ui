import request from '@/utils/request'

// 查询企业的服务理念列表
export function listConcept(query) {
  return request({
    url: '/website/concept/list',
    method: 'get',
    params: query
  })
}

// 查询企业的服务理念详细
export function getConcept(conceptId) {
  return request({
    url: '/website/concept/' + conceptId,
    method: 'get'
  })
}

// 新增企业的服务理念
export function addConcept(data) {
  return request({
    url: '/website/concept',
    method: 'post',
    data: data
  })
}

// 修改企业的服务理念
export function updateConcept(data) {
  return request({
    url: '/website/concept',
    method: 'put',
    data: data
  })
}

// 删除企业的服务理念
export function delConcept(conceptId) {
  return request({
    url: '/website/concept/' + conceptId,
    method: 'delete'
  })
}
