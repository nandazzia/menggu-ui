import request from '@/utils/request'

// 查询解决方案(子)列表
export function listSolution(query) {
  return request({
    url: '/website/solution/list',
    method: 'get',
    params: query
  })
}

// 查询解决方案(子)详细
export function getSolution(id) {
  return request({
    url: '/website/solution/' + id,
    method: 'get'
  })
}

// 新增解决方案(子)
export function addSolution(data) {
  return request({
    url: '/website/solution',
    method: 'post',
    data: data
  })
}

// 修改解决方案(子)
export function updateSolution(data) {
  return request({
    url: '/website/solution',
    method: 'put',
    data: data
  })
}

// 删除解决方案(子)
export function delSolution(id) {
  return request({
    url: '/website/solution/' + id,
    method: 'delete'
  })
}
