import request from '@/utils/request'

// 查询智能设备列表
export function listDevice(query) {
  return request({
    url: '/website/device/list',
    method: 'get',
    params: query
  })
}

// 查询智能设备详细
export function getDevice(id) {
  return request({
    url: '/website/device/' + id,
    method: 'get'
  })
}

// 新增智能设备
export function addDevice(data) {
  return request({
    url: '/website/device',
    method: 'post',
    data: data
  })
}

// 修改智能设备
export function updateDevice(data) {
  return request({
    url: '/website/device',
    method: 'put',
    data: data
  })
}

// 删除智能设备
export function delDevice(id) {
  return request({
    url: '/website/device/' + id,
    method: 'delete'
  })
}
