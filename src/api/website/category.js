import request from '@/utils/request'

// 查询类型分类列表
export function listCategory(query) {
  return request({
    url: '/category/category/list',
    method: 'get',
    params: query
  })
}

// 查询类型分类详细
export function getCategory(typeId) {
  return request({
    url: '/category/category/' + typeId,
    method: 'get'
  })
}

// 新增类型分类
export function addCategory(data) {
  return request({
    url: '/category/category',
    method: 'post',
    data: data
  })
}

// 修改类型分类
export function updateCategory(data) {
  return request({
    url: '/category/category',
    method: 'put',
    data: data
  })
}

// 删除类型分类
export function delCategory(typeId) {
  return request({
    url: '/category/category/' + typeId,
    method: 'delete'
  })
}
