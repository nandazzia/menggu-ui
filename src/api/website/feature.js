import request from '@/utils/request'

// 查询企业特色动画列表
export function listFeature(query) {
  return request({
    url: '/website/feature/list',
    method: 'get',
    params: query
  })
}

// 查询企业特色动画详细
export function getFeature(id) {
  return request({
    url: '/website/feature/' + id,
    method: 'get'
  })
}

// 新增企业特色动画
export function addFeature(data) {
  return request({
    url: '/website/feature',
    method: 'post',
    data: data
  })
}

// 修改企业特色动画
export function updateFeature(data) {
  return request({
    url: '/website/feature',
    method: 'put',
    data: data
  })
}

// 删除企业特色动画
export function delFeature(id) {
  return request({
    url: '/website/feature/' + id,
    method: 'delete'
  })
}
