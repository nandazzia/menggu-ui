import request from '@/utils/request'

// 查询产品亮点列表
export function listLights(query) {
  return request({
    url: '/website/lights/list',
    method: 'get',
    params: query
  })
}

// 查询产品亮点详细
export function getLights(id) {
  return request({
    url: '/website/lights/' + id,
    method: 'get'
  })
}

// 新增产品亮点
export function addLights(data) {
  return request({
    url: '/website/lights',
    method: 'post',
    data: data
  })
}

// 修改产品亮点
export function updateLights(data) {
  return request({
    url: '/website/lights',
    method: 'put',
    data: data
  })
}

// 删除产品亮点
export function delLights(id) {
  return request({
    url: '/website/lights/' + id,
    method: 'delete'
  })
}
