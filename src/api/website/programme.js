import request from '@/utils/request'

// 查询解决方案信息列表
export function listProgramme(query) {
  return request({
    url: '/website/programme/list',
    method: 'get',
    params: query
  })
}

// 查询解决方案信息详细
export function getProgramme(id) {
  return request({
    url: '/website/programme/' + id,
    method: 'get'
  })
}

// 新增解决方案信息
export function addProgramme(data) {
  return request({
    url: '/website/programme',
    method: 'post',
    data: data
  })
}

// 修改解决方案信息
export function updateProgramme(data) {
  return request({
    url: '/website/programme',
    method: 'put',
    data: data
  })
}

// 删除解决方案信息
export function delProgramme(id) {
  return request({
    url: '/website/programme/' + id,
    method: 'delete'
  })
}
